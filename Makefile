# Specify the Python interpreter
PYTHON := python

# Specify the manage.py command
MANAGE := $(PYTHON) manage.py

# Default target executed when no arguments are given to make.
default: run

# Target to start the Django development server.
run:
	@echo "Starting Django development server..."
	@$(MANAGE) runserver

# Target to create new migrations based on the changes detected to your models.
makemigrations:
	@echo "Creating new migrations..."
	@$(MANAGE) makemigrations

# Target to apply migrations
migrate:
	@echo "Applying migrations..."
	@$(MANAGE) migrate

createsuperuser:
	@echo "Creating superuser..."
	@$(MANAGE) createsuperuser

runtests:
	@echo "Creating superuser..."
	@$(MANAGE) test lettersgame

flush:
	@echo "Flush the database..."
	python manage.py flush

# Target to show help for each command.
help:
	@echo "Available commands:"
	@echo "  make run             - Starts the Django development server."
	@echo "  make makemigrations  - Creates new migrations based on changes to models."
	@echo "  make migrate         - Applies migrations to the database."
	@echo "  make createsuperuser - Creates super user."
	@echo "  make runtests        - runs the project test."
	@echo "  make flush           - Flush the database."
	@echo "  make help            - Displays help for commands."

