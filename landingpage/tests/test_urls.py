from django.test import SimpleTestCase
from django.urls import reverse, resolve

from landingpage.views import index


class TestUrls(SimpleTestCase):

    def test_define_dictionary_resolves(self):
        url = reverse('index')
        self.assertEqual(resolve(url).func, index)

    def test_define_dictionary_not_resolves(self):
        url = reverse('define_dictionary')
        self.assertNotEqual(resolve(url).func, index)
