from django.test import SimpleTestCase
from django.urls import reverse, resolve

from lettersgame.views import define_dictionary, start_game, validate_play


class TestUrls(SimpleTestCase):

    def test_define_dictionary_resolves(self):
        url = reverse('define_dictionary')
        self.assertEqual(resolve(url).func, define_dictionary)

    def test_define_dictionary_not_resolves(self):
        url = reverse('define_dictionary')
        self.assertNotEqual(resolve(url).func, start_game)

    def test_start_game_resolves(self):
        url = reverse('start_game')
        self.assertEqual(resolve(url).func, start_game)

    def test_start_game_not_resolves(self):
        url = reverse('start_game')
        self.assertNotEqual(resolve(url).func, define_dictionary)

    def test_validate_play_resolves(self):
        url = reverse('validate_play', args=[1])
        self.assertEqual(resolve(url).func, validate_play)

    def test_validate_play_not_resolves(self):
        url = reverse('validate_play', args=[1])
        self.assertNotEqual(resolve(url).func, start_game)
