import json

from django.test import TestCase, Client
from django.urls import reverse


def get_dictionary_dummy_data():
    return {
        "words": [
            "array", "arrays", "art", "arts", "fab", "fast", "fat", "fist", "lift", "lifts", "lire", "list",
            "load", "loaf", "loft", "lost", "lure", "lust", "rant", "rat", "rats", "rent", "rest", "rust",
            "sat", "soft", "sort", "sos", "soy", "start", "starts", "street", "tar", "tart", "tarts",
            "toll", "total", "toy", "toys", "tray", "trays"
        ]
    }


def get_start_game_dummy_data():
    return {
        "board": [
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P"
        ]
    }


class TestViews(TestCase):

    # Set up method
    def setUp(self):
        self.client = Client()
        self.define_dictionary_path = reverse('define_dictionary')
        self.start_game_path = reverse('start_game')

        # Common setup for define dictionary and start game
        self.client.post(self.define_dictionary_path, json.dumps(get_dictionary_dummy_data()),
                         content_type='application/json')
        board_response = self.client.post(self.start_game_path, json.dumps(get_start_game_dummy_data()),
                                          content_type='application/json')
        response_data = board_response.json()
        self.game_id = response_data['game_id']

    def test_define_dictionary_success(self):
        # Data to be sent with the request. Valid request should return 201
        data = get_dictionary_dummy_data()
        response = self.client.post(self.define_dictionary_path, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 201)

    def test_define_dictionary_fail(self):
        # Data to be sent with the request. Bad request should return 400
        data = {}
        response = self.client.post(self.define_dictionary_path, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_start_game_success(self):
        # Data to be sent with the request. Valid should return 200
        data = get_start_game_dummy_data()
        response = self.client.post(self.start_game_path, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 201)

    def test_start_game_fail(self):
        # Data to be sent with the request. Bad request should return 400
        data = {}
        response = self.client.post(self.start_game_path, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_start_game_fail_when_board_is_less_than_16(self):
        # Data to be sent with the request. Bad request should return 400
        data = {
            "board": [
                "L"
            ]
        }
        response = self.client.post(self.start_game_path, json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_validate_game_success(self):
        # Valid word
        data = {"word": "fab"}
        response = self.client.post(reverse('validate_play', args=[self.game_id]), json.dumps(data),
                                    content_type='application/json')
        response_json = response.json()
        valid_response_data = response_json['valid']

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTrue(valid_response_data)

    # This case for validating that the system should return false and actually retruned falce
    def test_validate_game_word_not_valid(self):
        # Not Valid word
        data = {"word": "STREET"}
        response = self.client.post(reverse('validate_play', args=[self.game_id]), json.dumps(data),
                                    content_type='application/json')
        response_json = response.json()
        valid_response_data = response_json['valid']

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertFalse(valid_response_data)
