from django.test import TestCase
from lettersgame.models import Game, Word, Dictionary


class TestModels(TestCase):

    def setUp(self):
        self.dictionary = Dictionary.objects.create()
        self.word1 = Word.objects.create(text="array")
        self.word2 = Word.objects.create(text="arrays")
        self.dictionary.words.add(self.word1, self.word2)

    def test_is_word_exist(self):
        # Test that is_word_exist correctly identifies existing words
        self.assertTrue(self.dictionary.is_word_exist(self.word1))
        self.assertFalse(self.dictionary.is_word_exist(Word(text="notexist")))

    def test_get_words(self):
        # Test that get_words returns all words
        words = list(self.dictionary.get_words())
        self.assertIn(self.word1, words)
        self.assertIn(self.word2, words)
        self.assertEqual(len(words), 2)

    def test_game_model(self):
        # Create a game instance with board data
        board_data = {"words": ["array", "arrays", "art", "arts", "fab", "fast"]}
        game = Game.objects.create(board=board_data)

        # Fetch the game from the database
        saved_game = Game.objects.get(id=game.id)

        # Assert that the saved data matches the provided data
        self.assertEqual(saved_game.board, board_data)

    def test_word_uniqueness(self):
        Word.objects.create(text="art")
        with self.assertRaises(Exception):
            # Trying to create another Word with the same text should raise an exception
            Word.objects.create(text="art")
