from django.urls import path
from . import views

urlpatterns = [
    path('define-dictionary/', views.define_dictionary, name='define_dictionary'),
    path('start-game/', views.start_game, name='start_game'),
    path('games/<int:game_id>/validate/', views.validate_play, name='validate_play'),
]
