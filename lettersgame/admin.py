from django.contrib import admin

from .models import Game, Dictionary, Word

# Register your models here.
admin.site.register(Game)
admin.site.register(Dictionary)
admin.site.register(Word)
