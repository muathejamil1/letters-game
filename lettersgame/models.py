from django.db import models


class Game(models.Model):
    board = models.JSONField()


class Word(models.Model):
    text = models.CharField(max_length=100, unique=True)


class Dictionary(models.Model):
    words = models.ManyToManyField(Word)

    def is_word_exist(self, word):
        return self.words.filter(id=word.id).exists()

    def get_words(self):
        return self.words.all()
