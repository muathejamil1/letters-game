from django.http import JsonResponse
from .models import Game, Dictionary, Word
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
import json


@csrf_exempt
@require_http_methods(["POST"])
def define_dictionary(request):
    try:
        data = json.loads(request.body)
        word_texts = set(data['words'])  # Ensures unique words only
    except (KeyError, json.JSONDecodeError):
        return JsonResponse({'error': 'Invalid JSON or missing words key.'}, status=400)

    # Process Words
    existing_words = Word.objects.filter(text__in=word_texts)
    existing_word_texts = set(existing_words.values_list('text', flat=True))
    new_words = [Word(text=text) for text in word_texts if text not in existing_word_texts]
    Word.objects.bulk_create(new_words)

    # Create new Dictionary and add Words
    new_dictionary = Dictionary()
    new_dictionary.save()
    new_dictionary.words.add(*existing_words, *Word.objects.filter(text__in=word_texts))

    return JsonResponse({'status': 'Dictionary created and words added'}, status=201)


@csrf_exempt
@require_http_methods(["POST"])
def start_game(request):
    try:
        data = json.loads(request.body)
        board = data['board']
    except (KeyError, json.JSONDecodeError):
        return JsonResponse({'error': 'Invalid JSON or missing board key.'}, status=400)

    if len(board) != 16:
        return JsonResponse({'error': 'Invalid board length. Length must be 16'}, status=400)
    # TODO: validate if all inputs are characters
    game = Game.objects.create(board=data['board'])
    return JsonResponse({'game_id': game.id}, status=201)


@csrf_exempt
@require_http_methods(["POST"])
def validate_play(request, game_id):
    data = json.loads(request.body)
    game = Game.objects.get(id=game_id)
    word = data['word']
    matrix = [game.board[i:i + 4] for i in range(0, len(game.board), 4)]
    valid = False
    # 1. validate word length
    if len(word) < 3:
        return JsonResponse({'error': 'Invalid word length. must be grater or equal to 3'}, 400)

    # get first dictionary from database
    dictionary = get_first_dictionary()

    # 2. validate if word exist in the dictionary words
    if dictionary is None or word.lower() not in dictionary["words"]:
        return JsonResponse({'error': 'Invalid word. word not exist in app dictionary'}, status=400)

    # 3. validate if word exist in the board
    if is_valid_word(matrix, word):
        valid = True
    return JsonResponse({'valid': valid}, status=200)


# Get the first dictionary
def get_first_dictionary():
    dictionary = Dictionary.objects.first()  # Retrieves the first dictionary entry from the database
    if dictionary is not None:
        words = list(dictionary.words.values_list('text', flat=True))  # Retrieve words for the dictionary
        response_data = {
            'id': dictionary.id,
            'words': words
        }
        return response_data
    else:
        return None


# Is valid word
def is_valid_word(matrix, word):
    rows, cols = len(matrix), len(matrix[0])
    directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

    def dfs(row, col, index, visited):
        if index == len(word):
            return True
        if not (0 <= row < rows and 0 <= col < cols):
            return False
        if (row, col) in visited:
            return False
        if matrix[row][col].lower() != word[index].lower():
            return False

        visited.add((row, col))
        for dr, dc in directions:
            if dfs(row + dr, col + dc, index + 1, visited):
                return True
        visited.remove((row, col))
        return False

    for r in range(rows):
        for c in range(cols):
            if matrix[r][c].lower() == word[0].lower():
                if dfs(r, c, 0, set()):
                    return True
    return False
