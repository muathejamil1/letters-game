# Letters Game

## Introduction
Letters Game is a Django-based web application that allows users to define a custom dictionary, start a new game, and validate words based on the defined dictionary. This project uses Django 5.0.4 and Python 3.10.10, and utilizes SQLite3 as the default database.

## Features
- **Define a Dictionary**: Users can create a dictionary that the game will use to validate words.
- **Start a New Game**: Initiates a new game session where words can be validated.
- **Validate Words**: Plays are valid if the tiles satisfy all these conditions:
- * Consecutive tiles are neighbors (including diagonals).
- * Tiles can only be used once in each play, but they can be used again in future plays.
- * The formed word is at least 3 letters.
- * The formed word is present in the app's dictionary.

## Getting Started

### Prerequisites
To run this project, you'll need:
- Python 3.10.10
- Django 5.0.4
- Git (optional, recommended for cloning the repository)

### Running the project
This project comes with unit tests for urls, models, and views. here I am testing both happy and bad scenarios.
- to run the test you can use 
- ```bash 
  make run 

### Testing
This project comes with unit tests for urls, models, and views. here I am testing both happy and bad scenarios.
- to run the test you can use 
- ```bash 
  make runtests

### Installation

1. **Clone the Repository (Optional)**
   If you have Git installed, you can clone the repository using the following command:
   ```bash
   git clone https://github.com/muathejamil1/lettersgame.git
   
### Commands
- you can use 
- ``` bash
  make help
- make help will list all the commands you can use

### Screenshots

Welcome to the Letters Game:
![Welcome to Letters Game](screenshots/welcome_screen.png "Welcome Screen")

Define Dictionary:
![Define Dictionary](screenshots/define_dictionary_screen.png "Define Dictionary Screen")

Start New Game:
![Start New Game](screenshots/start_game_screen.png "Start Game Screen")

Validate Game:
![Validate Game](screenshots/validate_game_screen.png "Validate Game Screen")


